#!/usr/bin/env bash

# Get Google Cloud Services access token
# https://gitlab.com/guided-explorations/gcp/configure-openid-connect-in-gcp/-/blob/main/run_gcp_sts.sh
# https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/

# Project-specific configurations
SERVICE_ACCOUNT_EMAIL="gitlab-hosting@mooncatrescue-25600.iam.gserviceaccount.com"
PROJECT_NUMBER="954426041611"
POOL_ID="gitlab"
PROVIDER_ID="gitlab"

# Turn GitLab OIDC JWT into Google Cloud Access Token
PAYLOAD=$(cat <<EOF
{
  "audience": "//iam.googleapis.com/projects/${PROJECT_NUMBER}/locations/global/workloadIdentityPools/${POOL_ID}/providers/${PROVIDER_ID}",
  "grantType": "urn:ietf:params:oauth:grant-type:token-exchange",
  "requestedTokenType": "urn:ietf:params:oauth:token-type:access_token",
  "scope": "https://www.googleapis.com/auth/cloud-platform",
  "subjectTokenType": "urn:ietf:params:oauth:token-type:jwt",
  "subjectToken": "${CI_JOB_JWT_V2}"
}
EOF
)

FEDERATED_TOKEN=$(curl -X POST "https://sts.googleapis.com/v1/token" \
  --header "Accept: application/json" \
  --header "Content-Type: application/json" \
  --data "${PAYLOAD}" \
  | jq -r '.access_token'
  )

ACCESS_TOKEN=$(curl -X POST "https://iamcredentials.googleapis.com/v1/projects/-/serviceAccounts/${SERVICE_ACCOUNT_EMAIL}:generateAccessToken" \
  --header "Accept: application/json" \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer ${FEDERATED_TOKEN}" \
  --data '{"scope": ["https://www.googleapis.com/auth/cloud-platform"]}' \
  | jq -r '.accessToken'
  )


# Deploy to Firebase
npm i firebase-tools
export FIREBASE_TOKEN="${ACCESS_TOKEN}"
npx firebase experiments:enable webframeworks
npx firebase deploy
