import React from 'react'
import { ethers } from 'ethers'
import type { NextPage, GetServerSideProps } from 'next'
import Head from 'next/head'
import MoonCatGrid from 'components/MoonCatGrid'
import EthereumAddress from 'components/EthereumAddress'
import { API_SERVER_ROOT, ADDRESS_DETAILS } from 'lib/util'
import { MoonCatData } from 'lib/types'
import { fetchEnsAddress } from 'wagmi/actions'

const ZWS = '\u200B'

interface Props {
  ownerAddress: `0x${string}`
  moonCats: Array<MoonCatData>
}

const OwnerDetailsPage: NextPage<Props> = ({ ownerAddress, moonCats }) => {
  let litterSummary: React.ReactNode
  if (ADDRESS_DETAILS[ownerAddress]?.type == 'pool') {
    // This is an NFT pool address
    litterSummary = (
      <>
        This is an NFT pool address; all of these MoonCats are up for adoption <em>right now</em>! If you&rsquo;re
        interested in adopting any of these {moonCats.length} MoonCats, head over to{' '}
        <a href={ADDRESS_DETAILS[ownerAddress].link} target="_blank" rel="noreferrer">
          the pool&rsquo;s website
        </a>{' '}
        to do so.
      </>
    )
  } else if (moonCats.length == 1) {
    litterSummary = <>This address is the proud owner of a MoonCat!</>
  } else if (moonCats.length > 0) {
    litterSummary = <>This address is the proud owner of {moonCats.length} MoonCats!</>
  } else {
    litterSummary = <>This address doesn&rsquo;t own any MoonCats, currently.</>
  }

  const pageTitle = `Owner Profile - ${ownerAddress}`

  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
      </Head>
      <div className="text-container">
        <h1 className="hero">Owner Profile</h1>
        <p style={{ textAlign: 'center', marginTop: '-1em', fontSize: '1.5rem' }} className="text-scrim">
          <EthereumAddress address={ownerAddress} mode="bar" />
        </p>
        <section className="card">{litterSummary}</section>
      </div>
      {moonCats.length > 0 && <MoonCatGrid moonCats={moonCats} />}
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const request = require('lib/request')

  if (typeof ctx.params == 'undefined') {
    console.error('Routing error')
    return { notFound: true }
  }
  const id = ctx.params.id
  if (Array.isArray(id) || typeof id == 'undefined' || id == 'undefined') {
    console.error('Routing error', id)
    return { notFound: true }
  }

  let targetAddress
  if (ethers.utils.isAddress(id)) {
    targetAddress = ethers.utils.getAddress(id) // Ensure address is in checksummed format
  } else {
    targetAddress = await fetchEnsAddress({ name: id })
  }

  interface OwnedMoonCat {
    rescueOrder: number
    catId: string
    collection: {
      name: string
      address: string
    }
  }

  let data = await request(`${API_SERVER_ROOT}/owned-mooncats/${targetAddress}`)
  let owned: OwnedMoonCat[] = JSON.parse(data.body.toString('utf8'))

  return {
    props: {
      ownerAddress: targetAddress,
      moonCats: owned.map((moonCat) => {
        return moonCat.rescueOrder
      }),
    },
  }
}

export default OwnerDetailsPage
