import React, { useState } from 'react'
import RandomMoonCatRow from 'components/RandomMoonCatRow'
import Icon from 'components/Icon'
import { ADDRESS_DETAILS, useFetchStatus } from 'lib/util'
import type { NextPage } from 'next'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { fetchEnsAddress } from 'wagmi/actions'
import Link from 'next/link'

const ZWS = '\u200B'

const Owners: NextPage = () => {
  const [addressInput, setInputAddress] = useState<string>('')
  const [errorMessage, setErrorMessage] = useState<string>('')
  const [fetchStatus, setFetchStatus] = useFetchStatus()
  const router = useRouter()

  async function doLookup() {
    setErrorMessage('')
    setFetchStatus('pending')
    if (addressInput.match(/^0x[0-9a-f]{40}$/i)) {
      // Valid hex address
      router.push(`/owners/${addressInput}`)
      return
    }

    let ensAddr = await fetchEnsAddress({ name: addressInput })
    if (typeof ensAddr != 'undefined' && ensAddr != null) {
      // Valid ENS name
      router.push(`/owners/${ensAddr}`)
      return
    }

    setFetchStatus('error')
    setErrorMessage('That is not a valid address')
  }

  const pageTitle = 'Owner Profiles'

  let fetchDisplay: React.ReactNode
  switch (fetchStatus) {
    case 'error': {
      fetchDisplay = (
        <p style={{ textAlign: 'center' }} className="message-error">
          <Icon name="warning" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
          {errorMessage}
        </p>
      )
      break
    }
    case 'pending': {
      fetchDisplay = <p style={{ textAlign: 'center' }}>Loading...</p>
      break
    }
  }

  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta name="description" property="og:title" content="Gallery view of individual Ethereum address holdings" />
      </Head>
      <div className="text-container">
        <h1 className="hero">Owner Profiles</h1>
        <RandomMoonCatRow />
        <section className="card">
          <p style={{ textAlign: 'center' }}>
            <input
              type="text"
              value={addressInput}
              onKeyDown={(e) => {
                if (e.key == 'Enter') doLookup()
              }}
              onChange={(e) => setInputAddress(e.target.value)}
              style={{ marginRight: '1em' }}
            />
            <button
              onClick={(e) => {
                e.preventDefault()
                doLookup()
              }}
            >
              Lookup
            </button>
          </p>
          {fetchDisplay}
          <p>Enter an Ethereum address (or ENS name) to look up that address&rsquo; information.</p>
        </section>
        <section className="card">
          <h2>NFT Pools</h2>
          <p>
            Not sure where to start browsing? Here&rsquo;s some addresses that are NFT pools, so all the MoonCats in
            them are up for adoption <em>right now</em>!
          </p>
          <ul>
            {Object.entries(ADDRESS_DETAILS)
              .filter(([k, v]) => {
                return v.type == 'pool'
              })
              .map(([k, v]) => {
                return (
                  <li key={k}>
                    <Link href={`/owners/${k}`}>{v.label}</Link>
                  </li>
                )
              })}
          </ul>
        </section>
      </div>
    </div>
  )
}
export default Owners
