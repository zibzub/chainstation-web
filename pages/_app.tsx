import 'styles/globals.scss'
import type { AppProps } from 'next/app'
import Head from 'next/head'
import BackgroundStars from 'components/BackgroundStars'
import Footer from 'components/Footer'
import Navigation from 'components/Navigation'
import { EthereumClient, modalConnectors, walletConnectProvider } from '@web3modal/ethereum'

import { Web3Modal } from '@web3modal/react'
import { configureChains, createClient, WagmiConfig } from 'wagmi'
import { arbitrum, mainnet, polygon } from 'wagmi/chains'
import { infuraProvider } from 'wagmi/providers/infura'
import { publicProvider } from 'wagmi/providers/public'

const chains = [mainnet, arbitrum, polygon]
const walletConnectProject = '705e98fb7f922815cbb7c1e82e0fbb5a'
// Wagmi client
const { provider } = configureChains(chains, [
  walletConnectProvider({ projectId: walletConnectProject }),
  infuraProvider({ apiKey: '9aa3d95b3bc440fa88ea12eaa4456161' }),
  publicProvider(),
])
const wagmiClient = createClient({
  autoConnect: true,
  connectors: modalConnectors({
    appName: 'MoonCatRescue Chainstation',
    chains,
  }),
  provider,
})

// Web3Modal Ethereum Client
const ethereumClient = new EthereumClient(wagmiClient, chains)

function ChainstationApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>MoonCatRescue</title>
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="MoonCatRescue Chainstation Alpha" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <WagmiConfig client={wagmiClient}>
        <Navigation />
        <div id="content">
          <Component {...pageProps} />
          <Footer />
        </div>
        <BackgroundStars />
      </WagmiConfig>
      <Web3Modal projectId={walletConnectProject} ethereumClient={ethereumClient} />
    </>
  )
}

export default ChainstationApp
