import { MoonCatData, MoonCatFilterSettings } from 'lib/types'
import type { NextApiRequest, NextApiResponse } from 'next'
import _rawTraits from 'lib/mooncat_traits.json'

const stringProps: string[] = ['classification', 'facing', 'expression', 'pattern', 'pose']

export type ResponseData = {
  length: number
  moonCats: MoonCatData[]
}

/**
 * Type predicate for MoonCatFilterSettings
 *
 * https://www.typescriptlang.org/docs/handbook/2/narrowing.html#using-type-predicates
 * Checks a bare object to ensure it follows the Type of a MoonCatFilterSettings object,
 * so it can be cast to it safely with Typescript after that point.
 */
function validateFilterSetings(arg: any): arg is MoonCatFilterSettings {
  if (typeof arg != 'object') return false

  for (let i = 0; i < stringProps.length; i++) {
    if (typeof arg[stringProps[i]] != 'undefined') {
      if (typeof arg[stringProps[i]] != 'string') return false
      if (arg[stringProps[i]].toLowerCase() != arg[stringProps[i]]) return false
    }
  }
  if (typeof arg.rescueYear != 'undefined' && typeof arg.rescueYear != 'number') return false

  return true
}

export default function handler(req: NextApiRequest, res: NextApiResponse<ResponseData>) {
  // Build MoonCatFilters object from query parameters
  const filters: any = {}
  stringProps.forEach((prop) => {
    if (typeof req.query[prop] != 'undefined' && !Array.isArray(req.query[prop])) {
      filters[prop] = req.query[prop]
    }
  })
  if (typeof req.query.rescueYear != 'undefined' && !Array.isArray(req.query.rescueYear)) {
    filters.rescueYear = parseInt(req.query.rescueYear)
  }
  if (!validateFilterSetings(filters)) return res.status(400).end()

  let totalList: MoonCatData[] = _rawTraits as MoonCatData[]
  if (typeof req.query.mooncats !== 'undefined' && req.query.mooncats !== 'all') {
    let moonCatArray = Array.isArray(req.query.mooncats) ? req.query.mooncats : req.query.mooncats.split(',')
    let requestedMoonCats = moonCatArray.map((str) => parseInt(str))
    totalList = totalList.filter((moonCat) => {
      return requestedMoonCats.includes(moonCat.rescueOrder)
    })
  }

  // Filter the list of MoonCats as the request indicates
  let filteredList = totalList.filter((moonCat) => {
    if (typeof filters.classification != 'undefined') {
      if (filters.classification == 'genesis' && moonCat.genesis == false) return false
      if (filters.classification == 'rescue' && moonCat.genesis == true) return false
    }
    if (typeof filters.facing != 'undefined') {
      if (filters.facing != moonCat.facing) return false
    }
    if (typeof filters.expression != 'undefined') {
      if (filters.expression != moonCat.expression) return false
    }
    if (typeof filters.pattern != 'undefined') {
      if (filters.pattern != moonCat.pattern) return false
    }
    if (typeof filters.pose != 'undefined') {
      if (filters.pose != moonCat.pose) return false
    }
    if (typeof filters.rescueYear != 'undefined') {
      if (filters.rescueYear != moonCat.rescueYear) return false
    }
    return true
  })

  // Slice to the desired offset and limit
  const limit = typeof req.query.limit == 'undefined' ? 50 : parseInt(String(req.query.limit))
  if (limit > 200) return res.status(400).end()
  const offset = typeof req.query.offset == 'undefined' ? 0 : parseInt(String(req.query.offset))

  res.status(200).json({ length: filteredList.length, moonCats: filteredList.slice(offset, offset + limit) })
}
