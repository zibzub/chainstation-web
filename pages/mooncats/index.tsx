import React from 'react'
import type { NextPage } from 'next'
import MoonCatGrid from 'components/MoonCatGrid'
import Head from 'next/head'

const ZWS = '\u200B'
const pageTitle = 'MoonCats'

const MoonCatsHome: NextPage = () => {
  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta
          name="description"
          property="og:description"
          content="Browse the entire collection of colorful astral felines"
        />
      </Head>
      <div className="text-container">
        <h1 className="hero">MoonCats</h1>
        <section className="card">
          <p>
            Discovered living on the moon back in 2017, they were <strong>rescued</strong> by many adventurous
            Etherians, and brought onto the blockchain. They now coexist happily with their human Etherian friends,
            serving as <em>companions</em> and <em>guides</em> into the uncharted <strong>Deep Space</strong>.
          </p>
        </section>
        <section className="card-help">
          <p>
            MoonCats are the primary NFT collection of the MoonCat{ZWS}Rescue project. The{' '}
            <a href="https://etherscan.io/address/0x60cd862c9c687a9de49aecdc3a99b74a4fc54ab6">original contract</a> was
            deployed in 2017, before &quot;NFT&quot; was an established term. They were <em>rescued</em>{' '}
            (&quot;minted&quot;) and brought to <em>Chainstation Alpha</em> (the Ethereum mainnet blockchain), and are
            the key characters guiding their human owners through <em>Deep Space</em> (the forward-looking possibilities
            of blockchain/web3 technologies).
          </p>
        </section>
      </div>
      <MoonCatGrid moonCats="all" />
    </div>
  )
}
export default MoonCatsHome
