import React from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'

const ZWS = '\u200B'
const pageTitle = 'Acclimator'
const Acclimator: NextPage = () => {
  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta
          name="description"
          property="og:description"
          content="Adapting MoonCats to be able to interact with the latest and greatest of Etherian technologies"
        />
      </Head>
      <div className="text-container">
        <h1 className="hero">Acclimator</h1>
        <section className="card">
          <p>
            <strong>Hard Hats Required!</strong> This area of Chainstation Alpha is undergoing some intense
            construction. So, please pardon the dust, and check back a bit later to see what pops up here!
          </p>
        </section>
        <section className="card-notice">
          <p>
            While this site is under construction, the Acclimator can be accessed{' '}
            <a href="https://mooncat.community/acclimator">on the MoonCat{ZWS}Community website</a>.
          </p>
        </section>
      </div>
    </div>
  )
}
export default Acclimator
