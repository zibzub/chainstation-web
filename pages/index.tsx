import RandomMoonCatRow from 'components/RandomMoonCatRow'
import type { NextPage } from 'next'
import Head from 'next/head'

const ZWS = '\u200B'

interface Props {}

const Home: NextPage<Props> = ({}) => {
  const pageTitle = `MoonCat${ZWS}Rescue Chainstation`
  return (
    <div id="content-container" className="text-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta
          name="description"
          property="og:description"
          content="Playground of the colorful felines rescued from the moon"
        />
      </Head>
      <h1 className="hero">{pageTitle}</h1>
      <section className="card-help">
        <p>
          MoonCats are an original NFT which launched in 2017 and helped pioneer <strong>on-chain generation</strong>,{' '}
          <strong>fair distribution</strong>, and <strong>user customization</strong>.{' '}
          <a href="https://mooncat.community">More info...</a>
        </p>
      </section>
      <RandomMoonCatRow />
      <section className="card-notice">
        <p>
          <strong>Caution! This site is in early beta phase.</strong> Not all the links work, and not all functionality
          is in place. The code repository for this web app is{' '}
          <a href="https://gitlab.com/mooncatrescue/chainstation-web">over here</a>; code and text contributions
          welcome!
        </p>
      </section>
      <section className="card-help">
        <h2>Welcome to the MoonCat{ZWS}Rescue Chainstation!</h2>
        <p>
          MoonCat{ZWS}Rescue sprouted an amazing <em>grass-roots community</em> of Rescuers, Adopters, Artists,
          Programmers, and Enthusiasts.{' '}
          <em>
            People rose to the challenge of reviving a mission half-finished and almost fully forgotten, and saw it
            through.
          </em>{' '}
          Though ponderware initially started the MoonCat{ZWS}Rescue mission, a wonderful community has continued to
          grow around it.
        </p>
        <p>
          The MoonCat{ZWS}Rescue team is grateful to have been welcomed into that community. Together, we hope to expand
          on the world of MoonCat{ZWS}Rescue, adding content and value to the ecosystem as a whole. We hope to{' '}
          <strong>explore, delight, and educate</strong> in the spirit of the original contract.
        </p>
      </section>
    </div>
  )
}
export default Home
