import React, { useState, useEffect } from 'react'
import { ethers } from 'ethers'
import { Address } from 'wagmi'

export const API_SERVER_ROOT = 'https://api.mooncat.community'

export const ADDRESS_DETAILS: { [key: Address]: { type: 'pool', label: string, link: string }} = {
  '0x98968f0747E0A261532cAcC0BE296375F5c08398': {
    type: 'pool',
    label: 'NFTX MOONCAT pool',
    link: 'https://nftx.io/vault/0x98968f0747e0a261532cacc0be296375f5c08398/buy/'
  },
  '0xA8b42C82a628DC43c2c2285205313e5106EA2853': {
    type: 'pool',
    label: 'NFTX MCAT17 pool',
    link: 'https://nftx.io/vault/0xa8b42c82a628dc43c2c2285205313e5106ea2853/buy/'
  },
  '0x67BDcD02705CEcf08Cb296394DB7d6Ed00A496F9': {
    type: 'pool',
    label: 'NFT20 CAT20 pool',
    link: 'https://nft20.io/asset/0x67bdcd02705cecf08cb296394db7d6ed00a496f9'
  }
}

export function sleep(delay: number): Promise<void> {
  return new Promise((resolve) => {
    window.setTimeout(() => {
      resolve()
    }, delay * 1000)
  })
}

/**
 * Given an array of items, add a separator between every one of them.
 * This is a replacement for Array.join() on a list of JSX elements
 */
export function interleave(arr: Array<JSX.Element | string>, glue: string = ', '): Array<JSX.Element | string> {
  let formatted: Array<JSX.Element | string> = []
  arr.forEach((field, index) => {
    formatted.push(field)
    if (index < arr.length - 1) formatted.push(glue)
  })
  return formatted
}

/**
 * Tracking the status of a remote call
 */
type FetchStatus = 'start' | 'pending' | 'done' | 'error'
export function useFetchStatus() {
  return useState<FetchStatus>('start')
}

/**
 * Tracking whether the component has gone through its first reneder or not.
 */
export function useIsMounted() {
  const [mounted, setMounted] = useState(false) // Start as false
  useEffect(() => setMounted(true), []) // On first run, set to true
  return mounted
}

/**
 * Custom hook to debounce a useEffect action.
 */
export function useDebounce(action: Function, monitoringVariables: React.DependencyList, debounceTime: number = 200) {
  let dependencies = [...monitoringVariables, action, debounceTime]
  useEffect(() => {
    const handle = setTimeout(action, debounceTime)
    return () => clearTimeout(handle)
  }, dependencies)
}

/**
 * Custom Hook for transforming a smart contract address into an EthersJS Contract object.
 */
export function useContract(address: string, etherscanKey: string) {
  const [contract, setContract] = useState<ethers.Contract | null>(null)
  const { status, data } = useDelayedFetch(
    'https://api.etherscan.io/api?module=contract&action=getabi&address=' + address + '&apikey=' + etherscanKey
  )
  useEffect(() => {
    if (status == 'start' || status == 'pending') return
    if (status == 'error') {
      console.error('Etherscan API failure', data)
      return
    }
    if (data.status != 1) {
      console.error('Etherscan API result failure', data)
      return
    }
    const abi = JSON.parse(data.result)
    console.debug('ABI result', address, abi)
    setContract(new ethers.Contract(address, abi))
  }, [status, data, address])
  return contract
}

interface FetchResult {
  status: FetchStatus
  data: any
}

/**
 * Custom Hook for fetching an HTTP result, but in an abort-able manner.
 * If the component that uses this hook gets unmounted, an AbortController is
 * used to stop the HTTP request in-flight
 */
export function useDelayedFetch(fetchURI: string) {
  const [fetchResult, setFetchResult] = useState<FetchResult>({ status: 'start', data: null })
  useEffect(() => {
    console.log('custom hook mount')
    const controller = new AbortController()
    const signal = controller.signal

    let handle = window.setTimeout(async () => {
      setFetchResult({ status: 'pending', data: null })
      let rs = await fetch(fetchURI, { signal })
      let data = rs.ok ? await rs.json() : null
      setFetchResult({ status: rs.ok ? 'done' : 'error', data })
    })
    return () => {
      console.log('custom hook unmount')
      controller.abort() // Stop fetch in-progress, if it exists
      window.clearTimeout(handle) // Clear any timeout handler, if it exists
    }
  }, [fetchURI])
  return fetchResult
}
