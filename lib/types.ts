import { Address } from "wagmi"

type MoonCatClassification = 'genesis' | 'rescue'
type Expression = 'smiling' | 'grumpy' | 'pouting' | 'shy'
type Facing = 'right' | 'left'
type Pattern = 'pure' | 'tabby' | 'spotted' | 'tortie'
type Pose = 'standing' | 'pouncing' | 'stalking' | 'sleeping'
type RescueYear = 2017 | 2018 | 2019 | 2020 | 2021
export interface MoonCatData {
  rescueOrder: number
  catId: string
  genesis: boolean
  pale: boolean
  facing: string
  expression: Expression
  pattern: Pattern
  pose: Pose
  rescueYear: RescueYear
  nameRaw?: string
  name?: string | true
}

interface ContractDetails {
  tokenId: number
  description: string
  address: Address
  capabilities: string[]
}

export interface AccessoryDetails {
  accessoryId: string // Note: integer expressed as string
  name: string,
  displayName: string
  visible: boolean
}

export interface MoonCatDetails {
  rescueIndex: number
  catId: string // 0x-prefixed Hex
  name: string | null
  expression: Expression
  pose: Pose,
  pattern: Pattern
  pale: boolean
  isPale: boolean
  facing: Facing

  rescuedBy?: Address
  rescueYear: number

  isAcclimated: boolean
  owner: Address
  lootprint: 'Claimed' | 'Lost'
  contract: ContractDetails
  accessories: AccessoryDetails[]

  classification: MoonCatClassification
  genesis: boolean
  genesisGroup?: number

  hue: string
  hueValue: number
  kInt: number
  kBin: string // Binary value
  glow: number[]

  litterId: string // Hexadecimal value
  litter: number[]
  litterSize: number
  onlyChild: boolean

  twinId: string // Hexadecimal value
  twinSet: number[]
  twinSetSize: number
  hasTwins: false

  mirrorId: string // Hexadecimal value
  mirrorSet: number[]
  hasMirrors: boolean
  mirrorSetSize: number

  cloneId: string // Hexadecimal value
  cloneSet: number[]
  cloneSetSize: number
  hasClones: boolean
}

export interface MoonCatFilterSettings {
  classification?: MoonCatClassification
  facing?: Facing
  expression?: Expression
  pattern?: Pattern
  pose?: Pose
  rescueYear?: RescueYear
}

export interface AccessoryData {
  id: number
}
