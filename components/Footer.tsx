import React from 'react'

const Footer = () => {
  const currentYear = new Date().getFullYear()
  return (
    <footer>
      <div>
        <a href="https://twitter.com/mooncatrescue" target="_blank" rel="noreferrer">
          Twitter
        </a>
      </div>
      <div>
        <a href="http://discord.gg/mooncats" target="_blank" rel="noreferrer">
          Discord
        </a>
      </div>
      <div>
        <a href="https://mooncatcommunity.medium.com/" target="_blank" rel="noreferrer">
          Medium
        </a>
      </div>
      <div>
        &copy;{currentYear}{' '}
        <a href="https://linktr.ee/mooncats" target="_blank" rel="noreferrer">
          MoonCatRescue
        </a>
      </div>
    </footer>
  )
}
export default Footer
