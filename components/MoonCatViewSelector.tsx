import React from 'react'

interface Props {
  selectedView: string
  onChange: Function
}

const MoonCatViewSelector = ({ selectedView, onChange }: Props) => {
  function handleOnChange(e: React.ChangeEvent<HTMLSelectElement>) {
    onChange(e.target.value)
  }
  return (
    <div>
      <label style={{ fontSize: '0.8rem' }}>
        View Style:
        <select value={selectedView} onChange={handleOnChange} style={{ marginLeft: '0.5rem' }}>
          <option value="accessorized">Accessorized</option>
          <option value="mooncat">MoonCat</option>
          <option value="face">MoonCat Face</option>
        </select>
      </label>
    </div>
  )
}
export default MoonCatViewSelector
