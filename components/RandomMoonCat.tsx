import React, { useState, useEffect } from 'react'
import Link from 'next/link'

const RandomMoonCat = (): JSX.Element => {
  const [rescueOrder, setRescueOrder] = useState<number | undefined>(undefined)
  useEffect(() => {
    setRescueOrder(Math.floor(Math.random() * 25440))
  }, [])

  if (typeof rescueOrder == 'undefined') return <></>

  let label = `MoonCat #${rescueOrder}`
  return (
    <Link href={`/mooncats/${rescueOrder}`} passHref legacyBehavior>
      <a title={label}>
        <img
          className="mooncat-image"
          alt={label}
          title={label}
          src={'https://api.mooncat.community/image/' + rescueOrder + '?scale=3&padding=10'}
        />
      </a>
    </Link>
  )
}
export default RandomMoonCat
