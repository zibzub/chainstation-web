import React, { CSSProperties, ChangeEventHandler, useCallback, useEffect, useState } from 'react'
import { MoonCatFilterSettings } from 'lib/types'
import { interleave } from 'lib/util'

/**
 * Define the structure of the filters to assemble into a form
 */
interface FieldMeta {
  label: string
  options: { [key: string]: string }
}
const filterMeta: { [key: string]: FieldMeta } = {
  classification: {
    label: 'Classification',
    options: {
      genesis: 'Genesis',
      rescue: 'Rescue',
    },
  },
  facing: {
    label: 'Facing',
    options: {
      right: 'Right',
      left: 'Left',
    },
  },
  expression: {
    label: 'Expression',
    options: {
      smiling: 'Smiling',
      grumpy: 'Grumpy',
      pouting: 'Pouting',
      shy: 'Shy',
    },
  },
  pattern: {
    label: 'Coat Pattern',
    options: {
      pure: 'Pure',
      tabby: 'Tabby',
      spotted: 'Spotted',
      tortie: 'Tortie',
    },
  },
  pose: {
    label: 'Pose',
    options: {
      standing: 'Standing',
      pouncing: 'Pouncing',
      stalking: 'Stalking',
      sleeping: 'Sleeping',
    },
  },
  rescueYear: {
    label: 'Rescue Year',
    options: {
      2017: '2017',
      2018: '2018',
      2019: '2019',
      2020: '2020',
      2021: '2021',
    },
  },
}

/**
 * Render an individual form field, using a HTML SELECT element as the control
 */
interface SelectFieldProps {
  fieldId: string
  labelStyle: CSSProperties
  currentValue: any
  onChange: ChangeEventHandler<HTMLSelectElement>
}
const SelectField = ({ fieldId, labelStyle, currentValue, onChange }: SelectFieldProps) => {
  const meta = filterMeta[fieldId]
  return (
    <div className="field-row">
      <div className="field-label" style={labelStyle}>
        {meta.label}
      </div>
      <div className="field-control">
        <select name={fieldId} value={currentValue} onChange={onChange}>
          <option value="">Any</option>
          {Object.keys(meta.options).map((optionValue) => {
            return (
              <option key={optionValue} value={optionValue}>
                {meta.options[optionValue]}
              </option>
            )
          })}
        </select>
      </div>
    </div>
  )
}

interface Props {
  id?: string
  currentFilters: MoonCatFilterSettings
  filteredCount: number
  totalCount: number
  onChange: (propName: keyof MoonCatFilterSettings, newValue: any) => void
}
const MoonCatFilters = ({ id, currentFilters, filteredCount, totalCount, onChange }: Props) => {
  const [isOpen, setOpen] = useState<boolean>(false)

  /**
   * Handle a change to one of the form elements
   */
  function handleChange(e: any) {
    const propName = e.target.name as keyof MoonCatFilterSettings
    const newValue = e.target.value
    onChange(propName, newValue)
  }

  /**
   * Set the visibility of the full filters form to the opposite of what it currently is
   */
  function toggleModal() {
    setOpen(!isOpen)
  }

  /**
   * Document-level event listener
   * For every key press, if it was the Escape key, close the filters form
   */
  const escListener = useCallback(
    (event: any) => {
      if (event.key == 'Escape') {
        setOpen(false)
      }
    },
    [setOpen]
  )

  /**
   * On mount, start listening for keypresses
   */
  useEffect(() => {
    document.addEventListener('keydown', escListener, false)
    return () => {
      document.removeEventListener('keydown', escListener, false)
    }
  }, [escListener])

  // Build UI labels for the current filter state
  let filterPhrases = []
  for (let filterName in currentFilters) {
    const meta = filterMeta[filterName]
    const currentValue = currentFilters[filterName as keyof MoonCatFilterSettings]
    if (typeof currentValue != 'undefined') {
      filterPhrases.push(
        <React.Fragment key={filterName}>
          {meta.label}: <em>{meta.options[currentValue]}</em>
        </React.Fragment>
      )
    }
  }
  let filterPhrase: Array<JSX.Element | string> = ['']
  if (filterPhrases.length > 0) {
    filterPhrase = interleave(filterPhrases, ', ')
    filterPhrase.push('. ')
  }
  let filterSize: React.ReactNode
  if (filteredCount < totalCount) {
    filterSize = `${filteredCount.toLocaleString()} of ${totalCount.toLocaleString()} MoonCats`
  } else if (totalCount == 1) {
    // Showing just one MoonCat? No label for this
  } else if (totalCount == 2) {
    filterSize = `Showing both MoonCats`
  } else {
    filterSize = `Showing all ${totalCount.toLocaleString()} MoonCats`
  }

  const modalStyle = isOpen ? {} : { display: 'none' }
  const labelStyle = { width: '200px' }
  return (
    <div id={id} style={{ position: 'relative', display: 'flex', alignItems: 'baseline' }}>
      <button onClick={toggleModal}>Filter</button>
      <div style={{ padding: '0 1rem', fontSize: '0.8rem' }}>
        {filterPhrase}
        {filterSize}
      </div>
      <div className="filter-modal" style={modalStyle}>
        <SelectField
          fieldId="classification"
          labelStyle={labelStyle}
          currentValue={currentFilters.classification}
          onChange={handleChange}
        />
        <SelectField
          fieldId="facing"
          labelStyle={labelStyle}
          currentValue={currentFilters.facing}
          onChange={handleChange}
        />
        <SelectField
          fieldId="expression"
          labelStyle={labelStyle}
          currentValue={currentFilters.expression}
          onChange={handleChange}
        />
        <SelectField
          fieldId="pattern"
          labelStyle={labelStyle}
          currentValue={currentFilters.pattern}
          onChange={handleChange}
        />
        <SelectField
          fieldId="pose"
          labelStyle={labelStyle}
          currentValue={currentFilters.pose}
          onChange={handleChange}
        />
        <SelectField
          fieldId="rescueYear"
          labelStyle={labelStyle}
          currentValue={currentFilters.rescueYear}
          onChange={handleChange}
        />
      </div>
    </div>
  )
}
export default MoonCatFilters
