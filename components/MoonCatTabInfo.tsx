import React from 'react'
import EthereumAddress from './EthereumAddress'
import { TabProps } from '../pages/mooncats/[id]'
import { MoonCatDetails } from 'lib/types'
import { API_SERVER_ROOT, ADDRESS_DETAILS } from '../lib/util'
import Icon from './Icon'
import Link from 'next/link'

function ucFirst(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1)
}

const LitterView = ({ details }: { details: MoonCatDetails }) => {
  if (details.onlyChild) {
    return (
      <p>
        This MoonCat is an only child (they have no &ldquo;littermates&rdquo;; MoonCats who have the exact same{' '}
        {details.hue} color (in the hue {details.hueValue} range) and pattern as them).
      </p>
    )
  }

  return (
    <>
      <h2>Litter</h2>
      <p>
        This MoonCat&rsquo;s litter (MoonCats who share the exact same {details.hue} color (in the hue{' '}
        {details.hueValue} range) and pattern) has {details.litterSize} MoonCats in it:
      </p>
      <div id="item-grid" style={{ margin: '1em 0' }}>
        {details.litter.map((rescueIndex) => {
          if (rescueIndex == details.rescueIndex) {
            return (
              <div key={rescueIndex} className="item-thumb highlight">
                <a href="#">
                  <div
                    className="thumb-img"
                    style={{ backgroundImage: `url(${API_SERVER_ROOT}/image/${rescueIndex}?scale=3&padding=5)` }}
                  />
                  <p>#{rescueIndex}</p>
                  <p>
                    <em>(This MoonCat)</em>
                  </p>
                </a>
              </div>
            )
          }
          let tag: React.ReactNode
          if (details.cloneSet.includes(rescueIndex)) {
            tag = (
              <p>
                <em>Clone</em>
              </p>
            )
          } else if (details.mirrorSet.includes(rescueIndex)) {
            tag = (
              <p>
                <em>Mirror</em>
              </p>
            )
          } else if (details.twinSet.includes(rescueIndex)) {
            tag = (
              <p>
                <em>Twin</em>
              </p>
            )
          }
          return (
            <div key={rescueIndex} className="item-thumb">
              <a href={'/mooncats/' + rescueIndex}>
                <div
                  className="thumb-img"
                  style={{ backgroundImage: `url(${API_SERVER_ROOT}/image/${rescueIndex}?scale=3&padding=5)` }}
                />
                <p>#{rescueIndex}</p>
                {tag}
              </a>
            </div>
          )
        })}
      </div>
    </>
  )
}

const MoonCatTabInfo = ({ moonCat, details }: TabProps) => {
  const owner = details.owner

  let marketplaceDetails: React.ReactNode
  if (details.isAcclimated) {
    marketplaceDetails = (
      <p>
        <a
          target="_blank"
          rel="noreferrer"
          href={`https://app.uniswap.org/#/nfts/asset/0xc3f733ca98e0dad0386979eb96fb1722a1a05e69/${moonCat.rescueOrder}`}
        >
          View this MoonCat on NFT marketplaces
          <Icon name="link-external" style={{ marginLeft: '0.5em', verticalAlign: '-0.2em' }} />
        </a>
      </p>
    )
  } else {
    marketplaceDetails = (
      <p>
        This MoonCat is currently held in the original MoonCatRescue contract, and therefore won&rsquo;t show up on most
        NFT marketplaces. Their owner might wrap them into an ERC721 token (&ldquo;Acclimate&rdquo; them) in the future, and after
        that point they would be visible in tools that enumerate ERC721 tokens.
      </p>
    )
  }

  let coatDetail = `${ucFirst(details.hue)} ${ucFirst(moonCat.pattern)}`
  if (!details.genesis && details.isPale) coatDetail = 'Pale ' + coatDetail

  let nameDetails: React.ReactNode
  if (typeof moonCat.name != 'undefined') {
    if (moonCat.name === true) {
      nameDetails = (
        <>
          This MoonCat is named <code>{moonCat.nameRaw}</code> (a raw bit of data; not a valid string name).
        </>
      )
    } else {
      nameDetails = (
        <>
          This MoonCat is named <em>{moonCat.name}</em>.
        </>
      )
    }
  } else {
    nameDetails = <>This MoonCat doesn&rsquo;t have a name yet. It&rsquo;s owner can choose to give it a name.</>
  }

  let genesisDetails
  if (details.genesis) {
    genesisDetails = (
      <p className="highlight">
        This is a <em>Genesis</em> MoonCat! These sort of MoonCats watch over all the other MoonCats. They weren&rsquo;t
        rescued by Ethereans like the other MoonCats, but willingly joined with the Ethereans in the rescue efforts.
        Genesis MoonCats joined the rescue operation in groups of 16 (this one was part of group {details.genesisGroup}
        ).
      </p>
    )
  }

  let ownerDetails: React.ReactNode
  if (typeof details.rescuedBy == 'undefined') {
    // This should only happen for Genesis MoonCats.
    if (ADDRESS_DETAILS[owner]?.type == 'pool') {
      // This MoonCat is in an NFT pool address
      const poolDetails = ADDRESS_DETAILS[owner]
      ownerDetails = (
        <>
          This MoonCat is in the <Link href={`/owners/${owner}`}>{poolDetails.label} pool</Link>; you could visit{' '}
          <Link href={poolDetails.link} target="_blank" rel="noreferrer">
            that pool&rsquo;s website
          </Link>{' '}
          and adopt them right now!
        </>
      )
    } else {
      ownerDetails = (
        <>
          This MoonCat is currently adopted by <EthereumAddress address={owner} href={`/owners/${owner}`} />.
        </>
      )
    }
  } else if (owner == details.rescuedBy) {
    // MoonCat is in the same address that rescued them.
    ownerDetails = (
      <>
        This MoonCat was rescued by{' '}
        <EthereumAddress address={details.rescuedBy} href={`/owners/${details.rescuedBy}`} />, and is still owned by
        that wallet.
      </>
    )
  } else {
    // MoonCat was rescued by one address, and currently owned by another; the most common situation.
    if (ADDRESS_DETAILS[owner]?.type == 'pool') {
      // This MoonCat is in an NFT pool address
      const poolDetails = ADDRESS_DETAILS[owner]
      ownerDetails = (
        <>
          This MoonCat was originally rescued by{' '}
          <EthereumAddress address={details.rescuedBy} href={`/owners/${details.rescuedBy}`} />, but currently is in the{' '}
          <em>{poolDetails.label}</em>; you could visit{' '}
          <a href={poolDetails.link} target="_blank" rel="noreferrer">
            that pool&rsquo;s website
          </a>{' '}
          and adopt them right now! Or browse <Link href={`/owners/${owner}`}>that pool&rsquo;s collection</Link> here
          on this site.
        </>
      )
    } else {
      ownerDetails = (
        <>
          This MoonCat was originally rescued by{' '}
          <EthereumAddress address={details.rescuedBy} href={`/owners/${details.rescuedBy}`} />, but has currently been
          adopted by <EthereumAddress address={owner} href={`/owners/${owner}`} />.
        </>
      )
    }
  }

  return (
    <div className="text-container">
      <section className="card-help">
        <p>
          This MoonCat has an identifier of <code>{moonCat.catId}</code>. This hexadecimal number is a bit like DNA in
          that it contains compressed information about this MoonCat&rsquo;s basic traits (color, facing, epression,
          pattern, and pose). Other traits have been gleaned by this MoonCat over time, as they and their human owner
          leave their mark on the blockchain.
        </p>
        {marketplaceDetails}
      </section>
      <section className="card">
        <ul style={{ margin: '0 0 1em' }} className="two-col">
          <li>
            <strong>Rescue Order</strong> #{moonCat.rescueOrder}
          </li>
          <li>
            <strong>Identifier</strong> {moonCat.catId}
          </li>
          <li>
            <strong>Facing</strong> {ucFirst(moonCat.facing)}
          </li>
          <li>
            <strong>Expression</strong> {ucFirst(moonCat.expression)}
          </li>
          <li>
            <strong>Coat</strong> {coatDetail}
          </li>
          <li>
            <strong>Pose</strong> {ucFirst(moonCat.pose)}
          </li>
          <li>
            <strong>Rescue Year</strong> {moonCat.rescueYear}
          </li>
        </ul>
        {genesisDetails}
        <p>
          {nameDetails} Once a MoonCat is named, it cannot be changed, and will be part of that MoonCat&rsquo;s traits
          forever after.
        </p>
        <p>{ownerDetails}</p>
        <LitterView details={details} />
      </section>
    </div>
  )
}
export default MoonCatTabInfo
