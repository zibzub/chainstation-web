import React from 'react'
import Link from 'next/link'
import { useEnsName, useEnsAvatar } from 'wagmi'
import { useIsMounted, ADDRESS_DETAILS } from 'lib/util'
import Icon from './Icon'

interface Props {
  address: `0x${string}`
  href?: string
  mode?: 'card' | 'bar' | 'short-bar' | 'mini'
  className?: string
  style?: React.CSSProperties
}

const EthereumAddress = ({ address, href, className, style, mode = 'short-bar' }: Props) => {
  const { data: ensName } = useEnsName({ address: address })
  const { data: avatar } = useEnsAvatar({ address: address })
  const isMounted = useIsMounted()

  let avatarView
  if (isMounted && typeof avatar != 'undefined' && avatar != null) {
    if (avatar.substring(0, 4) == 'http') {
      avatarView = <img className="eth-avatar" alt="" src={avatar} />
    }
  }

  let classes = 'eth-address eth-address-' + mode
  if (typeof className != 'undefined' && className != '') classes += ' ' + className

  const iconStyle: React.CSSProperties = { marginLeft: '0.5em', verticalAlign: '-0.2em' }

  switch (mode) {
    case 'bar':
    case 'short-bar':
      let label: React.ReactNode
      if (ADDRESS_DETAILS[address]) {
        label = <span className="address-label">{ADDRESS_DETAILS[address].label}</span>
      } else if (isMounted && typeof ensName != 'undefined' && ensName != null) {
        label = <span className="address-label">{ensName}</span>
      } else if (mode == 'short-bar') {
        label = (
          <span className="address-label">
            {address.substring(0, 8)}...{address.substring(address.length - 6)}
          </span>
        )
      } else {
        label = <span className="address-label hex-collapse">{address}</span>
      }

      let labelView
      if (typeof href != 'undefined' && href != '') {
        labelView = (
          <Link href={href} passHref legacyBehavior>
            <a style={{ lineHeight: '1em' }}>
              {avatarView}
              {label}
            </a>
          </Link>
        )
      } else {
        labelView = (
          <>
            {avatarView}
            {label}
          </>
        )
      }

      return (
        <span title={mode == 'short-bar' ? address : ''} className={classes} style={style}>
          {labelView}
          <a
            title="Copy address"
            className="alt-link"
            style={{ cursor: 'pointer' }}
            onClick={() => {
              navigator.clipboard.writeText(address)
            }}
          >
            <Icon name="copy" style={iconStyle} />
          </a>
          <a
            href={`https://etherscan.io/address/${address}`}
            className="alt-link"
            title="View on Etherscan"
            target="_blank"
            rel="noreferrer"
          >
            <Icon name="link-external" style={iconStyle} />
          </a>
        </span>
      )
    default:
      return <div>Unknown mode</div>
  }
}
export default EthereumAddress
