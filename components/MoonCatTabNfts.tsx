import React from 'react'
import { TabProps } from '../pages/mooncats/[id]'

const MoonCatTabNfts = ({ moonCat }: TabProps) => {
  return (
    <div className="text-container">
      <section className="card-help">
        MoonCats are able to contain/own other NFTs (following the ERC998 standard), which then stay with the MoonCat
        even if the MoonCat is moved to a different wallet. Any ERC721-compliant token can be given to a MoonCat (put in
        their Purrse).
      </section>
      <section className="card-notice">
        <p>
          <strong>Caution! This site is in early beta phase.</strong> While under construction, visit{' '}
          <a href={`https://purrse.mooncat.community/${moonCat.rescueOrder}#nfts/`}>The Purrse</a> to see their owned
          NFTs.
        </p>
      </section>
    </div>
  )
}
export default MoonCatTabNfts
