import React from 'react'
import { API_SERVER_ROOT } from '../lib/util'
import { TabProps } from '../pages/mooncats/[id]'

const MoonCatTabAccessories = ({ moonCat, details }: TabProps) => {
  const accessories = details.accessories.sort((a, b) => a.name.localeCompare(b.name))
  const totalOwned = accessories.length

  const documentation =
    'Accessories are additional visual enhancements that MoonCat owners can buy, ' +
    'and the MoonCat can then don/doff at any time. Once purchased, those accessories go with the MoonCat, ' +
    'even if the MoonCat changes to a new owner.'

  if (totalOwned == 0) {
    return (
      <div className="text-container">
        <section className="card-help">
          {documentation} This MoonCat currently doesn&rsquo;t own any accessories.
        </section>
      </div>
    )
  }
  return (
    <>
      <div className="text-container">
        <section className="card-help">
          {documentation} This MoonCat owns {totalOwned} accessories.
        </section>
      </div>
      <div id="item-grid" style={{ margin: '2rem 0' }}>
        {accessories.map((acc) => (
          <div className="item-thumb" key={acc.accessoryId}>
            <a href={'/accessories/' + acc.accessoryId}>
              <div
                className="thumb-img"
                style={{
                  backgroundImage: `url(${API_SERVER_ROOT}/image/${moonCat.catId}?acc=${acc.accessoryId}&scale=3&padding=5)`,
                }}
              />
              <p>{acc.name}</p>
            </a>
          </div>
        ))}
      </div>
    </>
  )
}
export default MoonCatTabAccessories
