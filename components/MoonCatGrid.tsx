import React, { useCallback, useEffect, useState } from 'react'
import { API_SERVER_ROOT } from '../lib/util'
import MoonCatFilters from './MoonCatFilters'
import MoonCatViewSelector from './MoonCatViewSelector'
import Pagination from './Pagination'
import Icon from './Icon'
import { MoonCatData, MoonCatFilterSettings } from '../lib/types'
import { useIsMounted, useFetchStatus } from '../lib/util'

const PREFERENCE_MOONCATVIEW_KEY = 'mooncatview'
const DEFAULT_PER_PAGE = 50

type MoonCatViewPreference = 'accessorized' | 'mooncat' | 'face' | null

interface ThumbProps {
  moonCat: MoonCatData
  viewStyle: MoonCatViewPreference
}

const MoonCatThumb = ({ moonCat, viewStyle }: ThumbProps) => {
  let thumbStyle: React.CSSProperties = {}
  switch (viewStyle) {
    case 'accessorized':
      thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5)`
      break
    case 'mooncat':
      thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5&acc=)`
      break
    case 'face':
      thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5&acc=&headOnly)`
      break
  }

  let name = null
  if (typeof moonCat.name != 'undefined') {
    if (moonCat.name === true) {
      name = (
        <p>
          <Icon name="question" />
        </p>
      )
    } else {
      name = <p>{moonCat.name}</p>
    }
  }

  return (
    <div className="item-thumb">
      <a href={'/mooncats/' + moonCat.rescueOrder}>
        <div className="thumb-img" style={thumbStyle} />
        <p>#{moonCat.rescueOrder}</p>
        <p>
          <code>{moonCat.catId}</code>
        </p>
        {name}
      </a>
    </div>
  )
}

interface FilterParams {
  pageNumber: number
  perPage: number
  moonCats: MoonCatData[] | 'all'
  filters: MoonCatFilterSettings
}

// Custom hook to encapsulate fetching data from local API endpoint
const useMoonCatPage = () => {
  const [status, setStatus] = useFetchStatus()
  const [moonCatPage, setMoonCatPage] = useState<MoonCatData[]>([])
  const [filteredSetSize, setFilteredSetSize] = useState<number>(0)

  // Call the local API endpoint to fetch a page of results
  const doFilter = useCallback(
    async function ({ pageNumber, perPage, moonCats, filters }: FilterParams) {
      setStatus('pending')
      let params = new URLSearchParams(filters as Record<string, string>)
      params.set('limit', String(perPage!))
      params.set('offset', String(pageNumber * perPage!))
      if (Array.isArray(moonCats)) {
        params.set('mooncats', moonCats.join(','))
      } else {
        params.set('mooncats', moonCats)
      }
      try {
        let searchRes = await fetch('/api/mooncats?' + params.toString())
        let data = await searchRes.json()
        setFilteredSetSize(data.length)
        setMoonCatPage(data.moonCats)
        setStatus('done')
      } catch (err) {
        console.error(err)
        setStatus('error')
      }
    },
    [setStatus]
  )

  return {
    status,
    moonCatPage,
    filteredSetSize,
    doFilter,
  }
}

interface Props {
  moonCats: Array<MoonCatData> | 'all'
  perPage?: number
}

const MoonCatGrid = ({ moonCats: allMoonCats, perPage }: Props) => {
  const totalMoonCats = Array.isArray(allMoonCats) ? allMoonCats.length : 25440
  const [viewPreference, setViewPreference] = useState<MoonCatViewPreference>(null)
  const isMounted = useIsMounted()
  if (typeof perPage === 'undefined') {
    perPage = DEFAULT_PER_PAGE
  }
  const [filterProps, setFilterProps] = useState<FilterParams>({
    pageNumber: 0,
    perPage: perPage,
    moonCats: allMoonCats,
    filters: {},
  })
  const { status, moonCatPage, filteredSetSize, doFilter } = useMoonCatPage()

  // Run once on mount, to fetch any existing preferences from the URL
  useEffect(() => {
    const params = new URLSearchParams(window.location.search)

    // Set MoonCat view style preference
    const queryPreference = params.get('view')
    if (queryPreference !== null && ['accessorized', 'mooncat', 'face'].includes(queryPreference.toLowerCase())) {
      // Query parameter takes priority
      setViewPreference(queryPreference as MoonCatViewPreference)
    } else {
      // Check local storage
      let savedPreference = localStorage.getItem(PREFERENCE_MOONCATVIEW_KEY) as MoonCatViewPreference
      if (savedPreference != null) {
        console.debug('Setting view preference:', savedPreference)
        setViewPreference(savedPreference)
      } else {
        console.debug('Setting default view preference')
        setViewPreference('accessorized')
      }
    }

    // Set currently-viewed page of data
    const pagePreference = params.get('page')
    if (pagePreference !== null) {
      let page = parseInt(pagePreference)
      if (page > 0) {
        // Page is valid; jump to it
        console.debug('Jumping to page:', page)
        setFilterProps((curProps) => {
          return { ...curProps, pageNumber: page - 1 }
        })
      }
    }
  }, [])

  // Event handler for updates from MoonCatFilter component when a user picks a new filter value
  function handleFilterUpdate(prop: keyof MoonCatFilterSettings, newValue: any) {
    setFilterProps((curProps) => {
      let newFilters = Object.assign({}, curProps.filters)
      if (newValue == '') {
        if (typeof curProps.filters[prop] == 'undefined') {
          // Already blank
          return curProps
        }
        delete newFilters[prop]
      } else {
        if (curProps.filters[prop] == newValue) {
          // Already set to that value
          return curProps
        }
        newFilters[prop] = newValue
      }

      return { ...curProps, filters: newFilters, pageNumber: 0 }
    })
  }

  // Event handler for updates from Pagination component when user navigates to a new page
  function handlePageUpdate(newPage: number) {
    setFilterProps((curProps) => {
      if (curProps.pageNumber == newPage) {
        // Already set to that value
        return curProps
      }

      return { ...curProps, pageNumber: newPage }
    })
  }

  // When filter parameters change, update URL and fetch updated page of MoonCats
  useEffect(() => {
    if (!isMounted) return // Skip on mount; only needed on update
    const params = new URLSearchParams(window.location.search)
    params.set('page', String(filterProps.pageNumber + 1)) // pageNumber is zero-based, but we show it one-based for human-friendliness
    window.history.replaceState({}, '', `${window.location.pathname}?${params}`)
    doFilter(filterProps)
  }, [isMounted, filterProps, doFilter])

  // When the view preference changes, save to local storage
  useEffect(() => {
    if (!isMounted) return // Skip on mount; only needed on update
    if (viewPreference != null) {
      localStorage.setItem(PREFERENCE_MOONCATVIEW_KEY, viewPreference)
    }
  }, [isMounted, viewPreference])

  return (
    <div className="mooncat-grid">
      <div
        style={{
          display: 'flex',
          alignItems: 'baseline',
          padding: '1rem 0',
        }}
        className="text-scrim"
      >
        {totalMoonCats > 1 && (
          <MoonCatFilters
            id="filters"
            currentFilters={filterProps.filters}
            filteredCount={filteredSetSize}
            totalCount={totalMoonCats}
            onChange={handleFilterUpdate}
          />
        )}
        <div style={{ flexGrow: 10 }} />
        {viewPreference != null && <MoonCatViewSelector selectedView={viewPreference} onChange={setViewPreference} />}
      </div>
      <div id="item-grid" style={{ margin: '0 0 2rem' }}>
        {moonCatPage.map((mc) => (
          <MoonCatThumb key={mc.rescueOrder} moonCat={mc} viewStyle={viewPreference} />
        ))}
      </div>
      <Pagination
        currentPage={filterProps.pageNumber}
        maxPage={Math.ceil(filteredSetSize / perPage) - 1}
        setCurrentPage={handlePageUpdate}
      />
    </div>
  )
}
export default MoonCatGrid
